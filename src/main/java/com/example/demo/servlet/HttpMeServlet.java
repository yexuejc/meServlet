package com.example.demo.servlet;

import com.example.demo.http.Request;
import com.example.demo.http.Response;

/**
 * 专门处理http的servlet的类
 *
 * @author maxf
 * @version 1.0
 * @ClassName HttpMeServlet
 * @Description
 * @date 2019/2/13 15:45
 */
public abstract class HttpMeServlet implements MeServlet {
    @Override
    public void service(Request request, Response response) {
        //判断请求方法
        if ("get".equalsIgnoreCase(request.getMethod())) {
            doGet(request, response);
        } else {
            doPost(request, response);
        }
    }

    public abstract void doGet(Request request, Response response);

    public abstract void doPost(Request request, Response response);

}
