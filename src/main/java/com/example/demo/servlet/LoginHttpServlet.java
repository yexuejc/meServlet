package com.example.demo.servlet;

import com.example.demo.http.Request;
import com.example.demo.http.Response;

import javax.servlet.ServletConfig;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author maxf
 * @version 1.0
 * @ClassName LoginHttpServlet
 * @Description
 * @date 2019/2/13 15:49
 */
public class LoginHttpServlet extends HttpMeServlet {
    @Override
    public void doGet(Request request, Response response) {
        this.doPost(request, response);
    }

    @Override
    public void doPost(Request request, Response response) {
        try {
            OutputStream outputStream = response.out;
            String s = Response.heard + " hello world!";
            outputStream.write(s.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void init(ServletConfig var1) throws IOException {

    }

    @Override
    public void destroy() {

    }
}
