package com.example.demo.servlet;

import com.example.demo.http.Request;
import com.example.demo.http.Response;

import javax.servlet.ServletConfig;
import java.io.IOException;

/**
 * 自定义接口
 *
 * @author maxf
 * @version 1.0
 * @ClassName MeServlet
 * @Description
 * @date 2019/2/13 15:43
 */
public interface MeServlet {
    void init(ServletConfig var1) throws IOException;

    void destroy();

    void service(Request request, Response response);
}
