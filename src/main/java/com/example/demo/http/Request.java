package com.example.demo.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * http请求的类
 *
 * @author maxf
 * @version 1.0
 * @ClassName Request
 * @Description
 * @date 2019/2/13 15:31
 */
public class Request {
    /**
     * 请求方式
     */
    private String method;
    /**
     * 访问地址
     */
    private String url;

    /**
     * 接收请求
     *
     * @param in
     * @throws IOException
     */
    public Request(InputStream in) throws IOException {
        //封装接收到的请求流
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        //将http请求的url和method读出来（第一行）
        String[] methodAndUrl = bufferedReader.readLine().split(" ");
        this.method = methodAndUrl[0];//get、post
        this.url = methodAndUrl[1];
    }

    public String getMethod() {
        return method;
    }

    public Request setMethod(String method) {
        this.method = method;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Request setUrl(String url) {
        this.url = url;
        return this;
    }
}
