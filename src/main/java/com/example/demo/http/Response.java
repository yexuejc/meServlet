package com.example.demo.http;

import java.io.IOException;
import java.io.OutputStream;

/**
 * http请求响应
 *
 * @author maxf
 * @version 1.0
 * @ClassName Resp
 * @Description
 * @date 2019/2/13 15:38
 */
public class Response {
    /**返回响应体*/
    public OutputStream out;
    /**http默认成功的响应*/
    public final static String  heard="HTTP/1.1 200 \r\nContent-Type: text/html \r\n\r\n";

    public Response(OutputStream out) throws IOException {
        this.out = out;
    }

}
